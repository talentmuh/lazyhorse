import React, { useEffect, useLayoutEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { ThemeProvider } from 'styled-components';
import { AppRouter } from './Router/Router';
import { theme } from './Theme/theme';
import { toast, ToastContainer } from 'react-toastify';
import { initProvider } from './GlobalState/User';
import { appInitializer } from './GlobalState/InitSlice';

function App() {
  const dispatch = useDispatch();

  const userTheme = useSelector((state) => {
    return state.user.theme;
  });

  useEffect(() => {
    dispatch(appInitializer());
  }, [dispatch]);

  useLayoutEffect(() => {
    dispatch(initProvider());
  }, [dispatch]);

  return (
    <ThemeProvider theme={theme}>
      <div className={'wraper ' + (userTheme === 'dark' ? 'greyscheme' : '')}>
        <AppRouter />
        <ToastContainer position={toast.POSITION.BOTTOM_LEFT} hideProgressBar={true} />
      </div>
    </ThemeProvider>
  );
}

export default App;
