import React, { memo } from 'react';
import { BrowserRouter as Router, Switch, Route, Redirect } from 'react-router-dom';
import * as Sentry from '@sentry/react';
import { connect } from 'react-redux';
import Home from '../Components/pages/home';
import Header from '../Components/menu/header';
import history from '../history';
import { ErrorPage } from '../Components/pages/ErrorPage';
const SentryEnhancedRoute = Sentry.withSentryRouting(Route);

const mapStateToProps = (state) => ({
  walletAddress: state.user.address,
  authInitFinished: state.appInitialize.authInitFinished,
});

const Component = ({ walletAddress, authInitFinished }) => {
  return (
    <Router history={history}>
      <Sentry.ErrorBoundary fallback={() => <ErrorPage />}>
        <Header />
        <Switch>
          <SentryEnhancedRoute exact path="/" component={Home} />
          <SentryEnhancedRoute path="/home" render={() => <Redirect to="/" />} />
        </Switch>
      </Sentry.ErrorBoundary>
    </Router>
  );
};

export const AppRouter = connect(mapStateToProps)(memo(Component));
