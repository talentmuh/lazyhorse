import React, {useState} from 'react';
import Select from 'react-select';
import Footer from '../components/Footer';

const Home = () => {
  const customStyles = {
    option: (base, state) => ({
      ...base,
      background: '#fff',
      color: '#333',
      borderRadius: state.isFocused ? '0' : 0,
      '&:hover': {
        background: '#eee',
      },
    }),
    menu: (base) => ({
      ...base,
      borderRadius: 0,
      marginTop: 0,
    }),
    menuList: (base) => ({
      ...base,
      padding: 0,
    }),
    control: (base, state) => ({
      ...base,
      padding: 2,
    }),
  };
  const options = [
    { value: 'lhrc', label: 'LazyHorse' },
    { value: 'lp', label: 'LPToken' },
  ]

  const [selectedToken, setSelectedToken] = useState("lhrc");

  const onChangeToken = (token) => {
    setSelectedToken(token.value);
  }
  return (
    <div>
      <section className="container no-bottom">
        <div className="dropdownSelect">
            <Select
              styles={customStyles}
              placeholder= "select the token"
              options= {options}
              // getOptionLabel={(option) => option.getOptionLabel}
              // getOptionValue={(option) => option.getOptionValue}
              defaultValue="lhtc"
              value={selectedToken}
              onChange={onChangeToken}
            />
          </div>
      </section>

      <Footer />
    </div>
  );
};
export default Home;
