import React, {memo, useEffect, useState} from 'react';
import Blockies from 'react-blockies';
import { useDispatch, useSelector } from 'react-redux';
import useOnclickOutside from 'react-cool-onclickoutside';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import {
  faSignOutAlt,
} from '@fortawesome/free-solid-svg-icons';
import { toast } from 'react-toastify';
import MetaMaskOnboarding from '@metamask/onboarding';
import { Modal, NavLink, Spinner } from 'react-bootstrap';

import {
  connectAccount,
  onLogout,
  // setTheme,
  setShowWrongChainModal,
  chainConnect,
} from '../../GlobalState/User';

const AccountMenu = function () {
  const dispatch = useDispatch();
  const [showpop, btn_icon_pop] = useState(false);

  const closePop = () => {
    btn_icon_pop(false);
  };
  const refpop = useOnclickOutside(() => {
    closePop();
  });
  const walletAddress = useSelector((state) => {
    return state.user.address;
  });
  const correctChain = useSelector((state) => {
    return state.user.correctChain;
  });
 
  const user = useSelector((state) => {
    return state.user;
  });
  const needsOnboard = useSelector((state) => {
    return state.user.needsOnboard;
  });

  const logout = async () => {
    dispatch(onLogout());
  };

  const connectWalletPressed = async () => {
    if (needsOnboard) {
      const onboarding = new MetaMaskOnboarding();
      onboarding.startOnboarding();
    } else {
      dispatch(connectAccount());
    }
  };

  const handleCopy = (code) => () => {
    navigator.clipboard.writeText(code);
    toast.success('Copied!');
  };

  useEffect(() => {
    let defiLink = localStorage.getItem('DeFiLink_session_storage_extension');
    if (defiLink) {
      try {
        const json = JSON.parse(defiLink);
        if (!json.connected) {
          dispatch(onLogout());
        }
      } catch (error) {
        dispatch(onLogout());
      }
    }
    if (
      localStorage.getItem('WEB3_CONNECT_CACHED_PROVIDER') ||
      window.ethereum ||
      localStorage.getItem('DeFiLink_session_storage_extension')
    ) {
      if (!user.provider) {
        if (window.navigator.userAgent.includes("Crypto.com DeFiWallet")) {
          dispatch(connectAccount(false, "defi"));
        } else {
          dispatch(connectAccount());
        }
      }
    }
    if (!user.provider) {
      if (window.navigator.userAgent.includes("Crypto.com DeFiWallet")) {
        dispatch(connectAccount(false, "defi"));
      }
    }

    // eslint-disable-next-line
  }, []);

  const onWrongChainModalClose = () => {
    dispatch(setShowWrongChainModal(false));
  };

  const onWrongChainModalChangeChain = () => {
    dispatch(setShowWrongChainModal(false));
    dispatch(chainConnect());
  };

  return (
    <div className="mainside">
      {!walletAddress && (
        <div className="connect-wal">
          <NavLink onClick={connectWalletPressed}>Connect Wallet</NavLink>
        </div>
      )}
      {walletAddress && !correctChain && !user.showWrongChainModal && (
        <div className="connect-wal">
          <NavLink onClick={onWrongChainModalChangeChain}>Switch network</NavLink>
        </div>
      )}
      {walletAddress && correctChain && (
        <div id="de-click-menu-profile" className="de-menu-profile">
          <span onClick={() => btn_icon_pop(!showpop)}>
            <Blockies seed={user.address} size={8} scale={4} />
          </span>
          {showpop && (
            <div className="popshow" ref={refpop}>
              <div className="d-wallet">
                <h4>My Wallet</h4>
                <div className="d-flex justify-content-between">
                  <span id="wallet" className="d-wallet-address">{`${walletAddress.substring(
                    0,
                    4
                  )}...${walletAddress.substring(walletAddress.length - 3, walletAddress.length)}`}</span>
                  <button className="btn_menu" title="Copy Address" onClick={handleCopy(walletAddress)}>
                    Copy
                  </button>
                </div>
              </div>
              <div className="d-wallet">
                <h4>Wallet Balance</h4>
                <div className="d-flex justify-content-between">
                  {!user.connectingWallet ? (
                    <span>{user.balance ? <>{Math.round(user.balance * 100) / 100} CRO</> : <>N/A</>}</span>
                  ) : (
                    <span>
                      <Spinner animation="border" role="status" size={'sm'}>
                        <span className="visually-hidden">Loading...</span>
                      </Spinner>
                    </span>
                  )}
                </div>
              </div>
              <div className="d-line"></div>
              <ul className="de-submenu-profile">
                <li>
                  <span onClick={logout}>
                    <span>
                      {' '}
                      <FontAwesomeIcon icon={faSignOutAlt} />{' '}
                    </span>
                    <span>Disconnect Wallet</span>
                  </span>
                </li>
              </ul>
            </div>
          )}
        </div>
      )}

      <Modal show={user.showWrongChainModal} onHide={onWrongChainModalClose}>
        <Modal.Header closeButton>
          <Modal.Title>Wrong network!</Modal.Title>
        </Modal.Header>
        <Modal.Body>
          To continue, you need to switch the network to <span style={{ fontWeight: 'bold' }}>{process.env.REACT_APP_CHAIN_NAME}</span>.{' '}
        </Modal.Body>
        <Modal.Footer>
          <button className="p-4 pt-2 pb-2 btn_menu inline white lead " onClick={onWrongChainModalClose}>
            Close
          </button>
          <button
            className="p-4 pt-2 pb-2 btn_menu inline white lead btn-outline"
            onClick={onWrongChainModalChangeChain}
          >
            Switch
          </button>
        </Modal.Footer>
      </Modal>
    </div>
  );
};

export default memo(AccountMenu);
